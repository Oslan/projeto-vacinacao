package com.projeto.vacinacao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.projeto.vacinacao.dto.AplicacaoVacinaDTO;
import com.projeto.vacinacao.dto.PessoaDTO;
import com.projeto.vacinacao.dto.VacinaDTO;
import com.projeto.vacinacao.entities.AplicacaoVacina;
import com.projeto.vacinacao.entities.Pessoa;
import com.projeto.vacinacao.entities.Vacina;
import com.projeto.vacinacao.exception.EntityNotFoundException;
import com.projeto.vacinacao.repositories.AplicacaoVacinaRepository;
import com.projeto.vacinacao.repositories.PessoaRepository;
import com.projeto.vacinacao.repositories.VacinaRepository;

@Service
public class AplicacaoVacinaService {

	@Autowired
	private AplicacaoVacinaRepository aplicacaoVacinaRepository;
	
	@Autowired
	private PessoaRepository usuarioRepository;
	
	@Autowired
	private VacinaRepository vacinaRepository;

	@Transactional
	public AplicacaoVacinaDTO save(AplicacaoVacinaDTO aplicacaoVacinaDTO) {
		
		Long idPessoa = aplicacaoVacinaDTO.getPessoaDTO().getId();
		Long idVacina = aplicacaoVacinaDTO.getVacinaDTO().getId();
		
		Optional<Pessoa> pessoaOp = this.usuarioRepository.findById(idPessoa);
		Pessoa pessoa = pessoaOp.orElseThrow(()-> new EntityNotFoundException("Pessoa nao encontrada!") );
		
		Optional<Vacina> vacinaOp = this.vacinaRepository.findById(idVacina);
		Vacina vacina = vacinaOp.orElseThrow(()-> new EntityNotFoundException("Vacina nao encontrada!") );
		
		AplicacaoVacina aplicacaoVacina = new AplicacaoVacina();
		aplicacaoVacina.setPessoa(pessoa);
		aplicacaoVacina.setVacina(vacina);
		
		
		aplicacaoVacina = this.aplicacaoVacinaRepository.save(aplicacaoVacina);
		
		AplicacaoVacinaDTO dto = new AplicacaoVacinaDTO();
		dto.setId(aplicacaoVacina.getId());
		dto.setPessoaDTO(new PessoaDTO(aplicacaoVacina.getPessoa()));
		dto.setVacinaDTO(new VacinaDTO(aplicacaoVacina.getVacina()));
		dto.setDataAplicacao(aplicacaoVacina.getDataAplicacao());
		
		return dto;
	}
	
	
}
