package com.projeto.vacinacao.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.projeto.vacinacao.dto.PessoaDTO;
import com.projeto.vacinacao.entities.Pessoa;
import com.projeto.vacinacao.repositories.PessoaRepository;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository usuarioRepository;
	
	public List<Pessoa> findAll(){
		return usuarioRepository.findAll();
	}

	@Transactional
	public PessoaDTO save(PessoaDTO usuarioDTO) {
		Pessoa usuario = new Pessoa();
		usuario.setNome(usuarioDTO.getNome());
		usuario.setCpf(usuarioDTO.getCpf());
		usuario.setDataNascimento(usuarioDTO.getDataNascimento());
		usuario.setEmail(usuarioDTO.getEmail());
		usuario = this.usuarioRepository.save(usuario);
		return new PessoaDTO(usuario);
	}
}
