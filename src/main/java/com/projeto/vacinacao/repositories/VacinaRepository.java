package com.projeto.vacinacao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projeto.vacinacao.entities.Vacina;


@Repository
public interface VacinaRepository extends JpaRepository<Vacina, Long> {

}
