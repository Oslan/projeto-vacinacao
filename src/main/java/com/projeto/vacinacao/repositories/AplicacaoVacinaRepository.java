package com.projeto.vacinacao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vacinacao.entities.AplicacaoVacina;

public interface AplicacaoVacinaRepository extends JpaRepository<AplicacaoVacina, Long> {

}
