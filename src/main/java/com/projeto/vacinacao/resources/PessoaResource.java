package com.projeto.vacinacao.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.projeto.vacinacao.dto.PessoaDTO;
import com.projeto.vacinacao.services.PessoaService;

@RestController
@RequestMapping(value="/usuarios")
public class PessoaResource {

	@Autowired
	private PessoaService pessoaService;
	
	
	@PostMapping
	public ResponseEntity<PessoaDTO> save(@Valid @RequestBody PessoaDTO pessoaDTO) {
		pessoaDTO = this.pessoaService.save(pessoaDTO);
		URI uri = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(pessoaDTO.getId())
				.toUri();
		
		return ResponseEntity.created(uri).body(pessoaDTO);
	}
}
