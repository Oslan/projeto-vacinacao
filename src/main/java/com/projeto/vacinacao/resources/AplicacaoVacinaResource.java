package com.projeto.vacinacao.resources;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.projeto.vacinacao.dto.AplicacaoVacinaDTO;
import com.projeto.vacinacao.services.AplicacaoVacinaService;

@RestController
@RequestMapping(value="/aplicacao")
public class AplicacaoVacinaResource {
	
	@Autowired
	private AplicacaoVacinaService aplicacaoVacinaService;

	@PostMapping
	public ResponseEntity<AplicacaoVacinaDTO> save(@RequestBody AplicacaoVacinaDTO aplicacaoVacinaDTO) {
		
		aplicacaoVacinaDTO = this.aplicacaoVacinaService.save(aplicacaoVacinaDTO);
		
		URI uri = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(aplicacaoVacinaDTO.getId())
				.toUri();
		
		return ResponseEntity.created(uri).body(aplicacaoVacinaDTO);
	}
}
