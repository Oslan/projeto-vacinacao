package com.projeto.vacinacao.resources.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationError extends StandardError {

	private static final long serialVersionUID = 1L;
	
	private List<FieldMessage> messages = new ArrayList<>();
	
	
	public ValidationError() {}


	public List<FieldMessage> getMessages() {
		return messages;
	}
	
	public void addError(String field, String message) {
		this.messages.add(new FieldMessage(field, message));
	}
	
	
}
