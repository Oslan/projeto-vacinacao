package com.projeto.vacinacao.dto;

import java.io.Serializable;

import com.projeto.vacinacao.entities.Vacina;

public class VacinaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String nome;
	
	public VacinaDTO() {
		
	}

	public VacinaDTO(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	public VacinaDTO(Vacina vacina) {
		this.id = vacina.getId();
		this.nome = vacina.getNome();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
