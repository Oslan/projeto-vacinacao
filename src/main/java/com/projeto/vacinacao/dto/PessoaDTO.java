package com.projeto.vacinacao.dto;

import java.io.Serializable;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.projeto.vacinacao.entities.Pessoa;

public class PessoaDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotBlank(message = "Nome obrigatório!")
	private String nome;
	
	@NotBlank(message = "Email obrigatório!")
	@Email(message = "Email inválido")
	private String email;
	
	@NotBlank(message = "CPF obrigatório!")
	private String cpf;
	
	@NotNull(message = "Data de nascimento obrigatória!")
	private Date dataNascimento;
	
	public PessoaDTO(){}
	
	public PessoaDTO(Long id, String nome, String email, String cpf, Date dataNascimento) {
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}
	
	public PessoaDTO(Pessoa usuario){
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.email = usuario.getEmail();
		this.cpf = usuario.getCpf();
		this.dataNascimento = usuario.getDataNascimento();
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	
}
