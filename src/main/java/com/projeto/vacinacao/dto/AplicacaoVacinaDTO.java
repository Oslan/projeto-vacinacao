package com.projeto.vacinacao.dto;

import java.io.Serializable;
import java.time.Instant;

public class AplicacaoVacinaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;

	private PessoaDTO pessoaDTO;
	
	private VacinaDTO vacinaDTO;
	
	private Instant dataAplicacao;
	
	
	public AplicacaoVacinaDTO() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PessoaDTO getPessoaDTO() {
		return pessoaDTO;
	}

	public void setPessoaDTO(PessoaDTO pessoaDTO) {
		this.pessoaDTO = pessoaDTO;
	}

	public VacinaDTO getVacinaDTO() {
		return vacinaDTO;
	}

	public void setVacinaDTO(VacinaDTO vacinaDTO) {
		this.vacinaDTO = vacinaDTO;
	}

	public Instant getDataAplicacao() {
		return dataAplicacao;
	}

	public void setDataAplicacao(Instant dataAplicacao) {
		this.dataAplicacao = dataAplicacao;
	}
	
	
	
}
